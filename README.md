# anpo.export_illustrator

Export layers from illustrator files. Available for a whole family of layers or just a specific layer asset. 

You must provide the ILLUSTRATOR_EXEC_PATH in the preferences of the site !
